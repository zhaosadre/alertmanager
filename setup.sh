#!/bin/sh
version=$1
package=$2
archive=$2.tar.gz
fingerprint=$3
set -ex
wget "https://github.com/prometheus/alertmanager/releases/download/v$version/$archive"
echo "$fingerprint  $archive" | sha256sum -c
tar --extract --file="$archive" "$package/alertmanager"
mv "$package/alertmanager" .
